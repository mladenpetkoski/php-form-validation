<!DOCTYPE HTML>  
<html>
<head>
<style>
.error {color: #FF0000;}
</style>
</head>
<body>  

<?php
    $name = $email = $website = $comment = $gender = $date ="";
    $nameErr = $emailErr = $genderErr = $websiteErr = $dateErr = "";
    
    if (isset($_GET['name'])) { $name = $_GET['name']; }
    if (isset($_GET['email'])) { $email = $_GET['email']; }
    if (isset($_GET['website'])) { $website = $_GET['website']; }
    if (isset($_GET['comment'])) { $comment = $_GET['comment']; }
    if (isset($_GET['gender'])) { $gender = $_GET['gender']; }
    if (isset($_GET['date'])) { $date = $_GET['date']; }

    if (isset($_GET['nameErr'])) { $nameErr = $_GET['nameErr']; }
    if (isset($_GET['emailErr'])) { $emailErr = $_GET['emailErr']; }
    if (isset($_GET['websiteErr'])) { $websiteErr = $_GET['websiteErr']; }
    if (isset($_GET['commentErr'])) { $commentErr = $_GET['commentErr']; }
    if (isset($_GET['genderErr'])) { $genderErr = $_GET['genderErr']; }
    if (isset($_GET['dateErr'])) { $dateErr = $_GET['dateErr']; } 
?>

<h2>PHP Form Validation Example</h2>
<p><span class="error">* neophodna polja.</span></p>
<form method="post" action="register2.php">  
  Name: <input type="text" name="name" value="<?php echo $name;?>">
  <span class="error">* <?php echo $nameErr;?></span>
  <br><br>
  E-mail: <input type="text" name="email" value="<?php echo $email;?>">
  <span class="error">* <?php echo $emailErr;?></span>
  <br><br>
  Website: <input type="text" name="website" value="<?php echo $website;?>">
  <span class="error"><?php echo $websiteErr;?></span>
  <br><br>
  Date of Birth: <input type="date" name="date" value="<?php echo $date;?>">
  <span class="error">* <?php echo $dateErr;?></span>
  <br><br>
  Comment: <textarea name="comment" rows="5" cols="40"><?php echo $comment;?></textarea>
  <br><br>
  Gender:
  <input type="radio" name="gender" <?php if (isset($gender) && $gender=="female") echo "checked";?> value="female">Female
  <input type="radio" name="gender" <?php if (isset($gender) && $gender=="male") echo "checked";?> value="male">Male
  <span class="error">* <?php echo $genderErr;?></span>
  <br><br>
  <input type="submit" name="submit" value="Submit">  
</form>

</body>
</html>